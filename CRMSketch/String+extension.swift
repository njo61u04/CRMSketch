//
//  String+extension.swift
//  CRMSketch
//
//  Created by Lionuser on 2024/4/9.
//

import Foundation

extension String {
    var inComingURLFix: String {
        if let atIndex = self.firstIndex(of: "/") {
            return String(self.suffix(from: atIndex))
        } else {
            return ""
        }
    }
    var inComingURL: String {
        if let firstSlash = self.firstIndex(of: "/") {
            let startIndex = self.index(after: firstSlash) // 跳过冒号
            if let secondSlash = self.lastIndex(of: "/") {
                return String(self.suffix(from: self.index(after: secondSlash)))
            } else {
                return String(self.suffix(from: startIndex))
            }
        } else {
            return ""
        }
    }
}
