//
//  SwiftUIView.swift
//  CRMSketch
//
//  Created by Lionuser on 2024/4/9.
//

import SwiftUI

struct SwiftUIView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

#Preview {
    SwiftUIView()
}
