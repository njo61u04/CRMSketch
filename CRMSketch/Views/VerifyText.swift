//
//  VerifyText2.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/10.
//

import SwiftUI

struct VerifyText: View {
    var body: some View {
        HStack {
            VStack(alignment: .leading){
                Text("雄獅註冊驗證信")
                    .contentStyle()
                Text("XXX你好")
                    .contentStyle()
                    .padding(.top, 24)
                HStack {
                    Text("驗證連結：")
                        .contentStyle()
                    Text("http://....")
                        .contentStyle()
                        .underline()
                        .foregroundColor(Color(red: 0.05, green: 0.6, blue: 1))
                }
            }
            .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
    
            Spacer()
        }.padding(.leading, 38)
    }
}

#Preview {
    VerifyText()
}

