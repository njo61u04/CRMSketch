//
//  RedStarText.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/10.
//

import SwiftUI

struct RedStarText: View {
    @Binding var contentText: String
    
    var body: some View {
        HStack{
            Text("*")
                .font(
                    Font.custom("Inter", size: 20)
                        .weight(.medium)
                )
                .foregroundStyle(Color(red: 0.95, green: 0.28, blue: 0.13))                .padding(.bottom, 6.27)
            Text(contentText)
                .contentStyle()
                .padding(.leading, -8)
        }
    }
}

#Preview {
    RedStarText(contentText: .constant("test123"))
}
