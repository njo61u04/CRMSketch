//
//  SubmitButton.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/8.
//

import SwiftUI

struct SubmitButton: View {
    var body: some View {
        Text("Submit")
            .otherButtonStyle()
    }
}

#Preview {
    SubmitButton()
}
