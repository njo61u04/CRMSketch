//
//  RegisterAppleButton.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/8.
//

import SwiftUI

struct AppleButton: View {
    @Binding var method: String
    
    var body: some View {
        Text("以 Apple 帳號" + method)
            .uniformButtonStyle()
    }
}
#Preview {
    AppleButton(method: .constant(""))
}
