//
//  RegisterLineButton.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/8.
//

import SwiftUI

struct LineButton: View {
    @Binding var method: String
    
    var body: some View {     
        Text("以 Line 帳號" + method)
            .uniformButtonStyle()
    }
}

#Preview {
    LineButton(method: .constant(""))
}
