//
//  RegisterEmail.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/8.
//

import SwiftUI

struct EmailButton: View {
    @Binding var method: String
    
    var body: some View {
        Text("使用 Email " + method)
            .uniformButtonStyle()
    }
}

#Preview {
    EmailButton(method: .constant(""))
}
