//
//  ResendButton.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/19.
//

import SwiftUI

struct ResendButton: View {
    var body: some View {
        Text("重新寄送驗證信")
            .otherButtonStyle()
    }
}

#Preview {
    ResendButton()
}
