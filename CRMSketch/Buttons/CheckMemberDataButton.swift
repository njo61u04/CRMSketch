//
//  CheckMemberDataButton.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/1/31.
//

import SwiftUI

struct CheckMemberDataButton: View {
    var body: some View {
        Text("查看我的會員資料")
            .otherButtonStyle()
    }
}

#Preview {
    CheckMemberDataButton()
}
