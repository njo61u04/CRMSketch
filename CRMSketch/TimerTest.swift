//
//  TimerTest.swift
//  CRMSketch
//
//  Created by Lionuser on 2024/4/15.
//

import SwiftUI
import BackgroundTasks

struct TimerTest: View {
    
    @State private var elapsedTime: TimeInterval = 0
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        VStack {
            Text("Elapsed Time: \(formattedTime(elapsedTime))")
                .padding()
        }
        .onReceive(timer) { _ in
            elapsedTime += 1
        }
        .onAppear {
            scheduleBackgroundTask()
        }
    }
    
    private func formattedTime(_ time: TimeInterval) -> String {
        let minutes = Int(time) / 60
        let seconds = Int(time) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    private func scheduleBackgroundTask() {
        let request = BGProcessingTaskRequest(identifier: "com.example.BackgroundTimerApp")
        request.requiresNetworkConnectivity = false
        request.requiresExternalPower = false
        do {
            try BGTaskScheduler.shared.submit(request)
        } catch {
            print("Unable to submit background task: \(error)")
        }
    }
}

#Preview {
    TimerTest()
}
