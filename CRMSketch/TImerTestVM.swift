//
//  TImerTestVM.swift
//  CRMSketch
//
//  Created by Lionuser on 2024/4/15.
//

import SwiftUI
import BackgroundTasks

class TImerTestVM: ObservableObject {
    func scheduleAppRefresh() {
        let request = BGAppRefreshTaskRequest(identifier: "timer")
        try? BGTaskScheduler.shared.submit(request)
    }
}
