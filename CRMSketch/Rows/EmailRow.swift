//
//  EmailRow.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/8.
//

import SwiftUI

struct EmailRow: View {
    @Binding var email: String
    
    var body: some View {
        HStack {
            Text("Email")
                .contentStyle()
                .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
            
            TextField("請輸入信箱", text: $email, axis: .vertical)
                .lineLimit(1)
                .padding(.leading, 20)
                .frame(width: 201, height: 30.15)
                .textFieldStyle(GrayBorder())
                .keyboardType(.emailAddress)
                .textInputAutocapitalization(.never)
                .font(Font.custom("Inter", size: 10))
                .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))
        }
    }
}

#Preview {
    EmailRow(email: .constant(""))
}
