//
//  BirthdayRow.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/10.
//

import SwiftUI

struct BirthDateRow: View {
    @State private var showDatePicker = false
    @Binding var date: Date
    
    var body: some View {
//        VStack {
            HStack {
                RedStarText(contentText: .constant("生日"))
                    .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
                    .padding(.leading, 9)
                
                ZStack {
                    ZStack {
                        DatePicker("", selection: $date, in: ...Date(), displayedComponents: .date)
                            .datePickerStyle(.compact)
                            .labelsHidden()
                            .scaleEffect(0.5)
                        
                        SwiftUIWrapper {
                            Image("chevonDown")
                                .frame(width: 15, height: 15)
                                .padding(.leading, 40).allowsHitTesting(false)
                        }
                        .allowsHitTesting(false)
                    }
                    .frame(width: 0, height: 0)
                    
                    Text(date.description.yearFix)
                        .padding(.trailing, 27.5)
                        .frame(width: 68.12, height: 30)
                        .overlay(
                            RoundedRectangle(cornerRadius: 3.76785)
                                .stroke(Color(red: 0.8, green: 0.82, blue: 0.88), lineWidth: 1.25)
                        )
                }
                .padding(.leading, 17)
                
                ZStack {
                    ZStack {
                        DatePicker("", selection: $date, in: ...Date(), displayedComponents: .date)
                            .datePickerStyle(.compact)
                            .labelsHidden()
                            .scaleEffect(0.5)
                        
                        SwiftUIWrapper {
                            Image("chevonDown")
                                .frame(width: 15, height: 15)
                                .padding(.leading, 40)
                                .allowsHitTesting(false)
                        }
                        .allowsHitTesting(false)
                    }
                    .frame(width: 0, height: 0)
                    
                    Text(date.description.monthFix)
                        .padding(.trailing, 27.5)
                        .frame(width: 68.12, height: 30)
                        .overlay(
                            RoundedRectangle(cornerRadius: 3.76785)
                                .stroke(Color(red: 0.8, green: 0.82, blue: 0.88), lineWidth: 1.25)
                        )
                }
                .padding(.leading, 22)
                
                ZStack {
                    ZStack {
                        DatePicker("", selection: $date, in: ...Date(), displayedComponents: .date)
                            .datePickerStyle(.compact)
                            .labelsHidden()
                        
                        SwiftUIWrapper {
                            Image("chevonDown")
                                .frame(width: 15, height: 15)
                                .padding(.leading, 40).allowsHitTesting(false)
                        }
                        .allowsHitTesting(false)
                    }
                    .frame(width: 0, height: 0)
                    
                    Text(date.description.dayFix)
                        .padding(.trailing, 27.5)
                        .frame(width: 68.12, height: 30)
                        .overlay(
                            RoundedRectangle(cornerRadius: 3.76785)
                                .stroke(Color(red: 0.8, green: 0.82, blue: 0.88), lineWidth: 1.25)
                        )
                }
                .padding(.leading, 22)
                
                Spacer()
            }
            .font(Font.custom("Inter", size: 10))
            .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))
            
//            Text(date.description.formatFix)
//        }
    }
    
    struct SwiftUIWrapper<T: View>: UIViewControllerRepresentable {
        let content: () -> T
        func makeUIViewController(context: Context) -> UIHostingController<T> {
            UIHostingController(rootView: content())
        }
        func updateUIViewController(_ uiViewController: UIHostingController<T>, context: Context) {
        }
  }

}

#Preview {
    BirthDateRow(date: .constant(Date(timeInterval: -180000000, since: Date.now)))
}
