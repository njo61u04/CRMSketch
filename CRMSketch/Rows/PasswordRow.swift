//
//  PasswordLoginRow.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/19.
//

import SwiftUI

struct PasswordRow: View {
    @Binding var password: String
    
    var body: some View {
        HStack {
            Text("密碼")
                .contentStyle()
                .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
            
            TextField("請輸入密碼", text: $password, axis: .vertical)
                .lineLimit(1)
                .padding(.leading, 20)
                .frame(width: 201, height: 30.15)
                .textFieldStyle(GrayBorder())
                .keyboardType(.emailAddress)
                .textInputAutocapitalization(.never)
                .font(Font.custom("Inter", size: 10))
                .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))
        }
        .padding(.horizontal, 47)
    }
}

#Preview {
    PasswordRow(password: .constant(""))
}
