//
//  ConfirmPasswordRow.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/1/30.
//

import SwiftUI

struct ConfirmPasswordRow: View {
    @Binding var password: String
    
    var body: some View {
        HStack {
            Text("確認新密碼")
                .contentStyle()
                .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
            
            TextField("請輸入密碼", text: $password, axis: .vertical)
                .lineLimit(1)
                .padding(.leading, 8)
                .textFieldStyle(GrayBorder())
                .keyboardType(.emailAddress)
                .textInputAutocapitalization(.never)
                .font(Font.custom("Inter", size: 10))
                .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))
        }
        .padding(.leading, 14)
        .padding(.trailing, 33)
    }
}

#Preview {
    ConfirmPasswordRow(password: .constant("123"))
}
