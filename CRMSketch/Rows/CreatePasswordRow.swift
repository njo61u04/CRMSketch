//
//  PasswordRow.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/10.
//

import SwiftUI

struct CreatePasswordRow: View {
    @Binding var password: String
    
    var body: some View {
        HStack {
            RedStarText(contentText: .constant("密碼"))
                .padding(.leading, 9)
            
            TextField("", text: $password)
                .lineLimit(1)
                .padding(.leading, 15)
                .padding(.trailing, 19)
                .textFieldStyle(GrayBorder())
                .keyboardType(.default)
                .textInputAutocapitalization(.never)
                .font(Font.custom("Inter", size: 10))
                .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))
        }
    }
}

#Preview {
    CreatePasswordRow(password: .constant("123"))
}
