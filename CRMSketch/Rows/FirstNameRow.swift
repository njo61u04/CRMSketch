//
//  FirstnameRow.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/10.
//

import SwiftUI

struct FirstNameRow: View {
    @Binding var firstName: String
    
    var body: some View {
        HStack {
            RedStarText(contentText: .constant("名"))
                .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
                .padding(.leading, 9)
            
            TextField("", text: $firstName)
                .lineLimit(1)
                .padding(.leading, 40)
                .padding(.trailing, 19)
                .textFieldStyle(GrayBorder())
                .keyboardType(.default)
                .textInputAutocapitalization(.never)
                .font(Font.custom("Inter", size: 10))
                .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))
        }
    }
}

#Preview {
    FirstNameRow(firstName: .constant("小明"))
}
