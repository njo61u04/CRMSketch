//
//  AddressRow.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/10.
//

import SwiftUI

struct AddressRow: View {
    @Binding var addressCountry: String
    @Binding var addressCity: String
    @Binding var addressDetail: String
    
    var body: some View {
        HStack (alignment: .top){
            Text("地址")
                .contentStyle()
                .padding(.leading, 20)
                .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
            
            .padding(.top, 2.73)
            
            VStack (alignment: .leading){
                HStack{
                    ZStack {
                        
                        TextField("", text: $addressCountry)
                            .frame(width: 68.12)
                            .textFieldStyle(GrayBorder())
                        
                        Menu {
                            Button {
                                addressCountry = "台灣"
                            } label: {
                                Text("台灣")
                            }
                            Button {
                                addressCountry = "中國"
                            } label: {
                                Text("中國")
                            }
                        } label: {
                            Image("Icon")
                                .frame(width: 15, height: 15)
                                .padding(.leading, 40)
                        }
                    }
                    TextField("", text: $addressCity)
                        .textFieldStyle(GrayBorder())
                        .padding(.trailing, 19)
                        .textInputAutocapitalization(.never)
                }
                .padding(.top, 2.5)
                
                TextField("", text: $addressDetail)
                    .textFieldStyle(GrayBorder())
                    .padding(.trailing, 19)
                    .textInputAutocapitalization(.never)
            }
            .padding(.leading, 15)
            .font(Font.custom("Inter", size: 10))
            .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))
            
            Spacer()
        }
    }
}

#Preview {
    AddressRow(addressCountry: .constant("台灣"), addressCity: .constant("台北市"), addressDetail: .constant("...."))
}
