//
//  CountryRow.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/10.
//

import SwiftUI

struct CountryRow: View {
    @Binding var country: String
    @State private var showActionSheet = false
    
    var body: some View {
        HStack {

            Text("國籍")
                .contentStyle()
                .padding(.leading, 20)
                .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
            
            ZStack {
                TextField("", text: $country)
                    .frame(width: 68.12)
                    .textFieldStyle(GrayBorder())
                
                /*Button*/Menu {
                    Button {
                        country = "台灣"
                    } label: {
                        Text("台灣")
                    }
                    Button {
                        country = "中國"
                    } label: {
                        Text("中國")
                    }
//                    showActionSheet.toggle()
                } label: {
                    Image("Icon")
                        .frame(width: 15, height: 15)
                        .padding(.leading, 40)
                }
            }
            .padding(.leading, 15)
            .font(Font.custom("Inter", size: 10))
            .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))

            Spacer()
        }
    }
}

#Preview {
    CountryRow(country: .constant("台灣"))
}
