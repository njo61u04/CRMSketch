//
//  PhoneRow.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/10.
//

import SwiftUI

struct PhoneRow: View {
    @Binding var phoneNumber: String
    @Binding var internationalCode: String
    
    var body: some View {
        HStack {
            RedStarText(contentText: .constant("手機"))
                .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
                .padding(.leading, 9)
            
            HStack {
                ZStack {
                    TextField("", text: $internationalCode)
                        .frame(width: 68.12)
                        .textFieldStyle(GrayBorder())
                    
                    Menu {
                        Button {
                            internationalCode = "+886"
                        } label: {
                            Text("+886")
                        }
                        Button {
                            internationalCode = "+86"
                        } label: {
                            Text("+86")
                        }
                        Button {
                            internationalCode = "+852"
                        } label: {
                            Text("+852")
                        }
                        Button("+853") { internationalCode = "+853"}
                        
                    } label: {
                        Image("chevonDown")
                            .frame(width: 15, height: 15)
                            .padding(.leading, 40)
                    }
                }
                .padding(.leading, 16)
                
                TextField("", text: $phoneNumber)
                    .lineLimit(1)
                    .padding(.leading, -4)
                    .padding(.trailing, 19)
                    .textFieldStyle(GrayBorder())
                    .keyboardType(.default)
                    .textInputAutocapitalization(.never)
            }
            .font(Font.custom("Inter", size: 10))
            .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))
        }

    }
}

#Preview {
    PhoneRow(phoneNumber: .constant("0987654321"), internationalCode: .constant("+886"))
}
