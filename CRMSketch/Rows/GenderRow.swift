//
//  SexualRow.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/10.
//

import SwiftUI

struct GenderRow: View {
    @Binding var sexual: String
    @State private var showActionSheet = false
    
    var body: some View {
        HStack {
            Text("性別")
                .contentStyle()
                .padding(.leading, 20)
                .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
            
            ZStack {
                
                TextField("", text: $sexual)
                    .frame(width: 68.12)
                    .textFieldStyle(GrayBorder())
                
                Button/*Menu*/ {
//                    Button {
//                        sexual = "男"
//                    } label: {
//                        Text("男")
//                    }
//                    Button {
//                        sexual = "女"
//                    } label: {
//                        Text("女")
//                    }
                    showActionSheet.toggle()
                } label: {
                    Image("Icon")
                        .frame(width: 15, height: 15)
                        .padding(.leading, 40)
                }
            }
            .padding(.leading, 15)
            .font(Font.custom("Inter", size: 10))
            .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))
            .actionSheet(isPresented: $showActionSheet) {
                ActionSheet(title: Text("請選擇性別"), buttons: [
                    .default(Text("男"), action: {sexual = "男"}),
                    .default(Text("女"), action: {sexual = "女"}),
                    .cancel(Text("Cancel")
                        .foregroundColor(Color.black))
                ])
            }
            Spacer()
        }
    }
}

#Preview {
    GenderRow(sexual: .constant("男"))
}
