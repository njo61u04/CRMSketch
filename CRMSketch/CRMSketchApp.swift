//
//  CRMSketchApp.swift
//  CRMSketch
//
//  Created by LTC-04410-0016 on 2024/2/6.
//

import SwiftUI

@main
struct CRMSketchApp: App {
    var body: some Scene {
        WindowGroup {
            HomePage()
        }
    }
}
