//
//  ContentView.swift
//  CRMSketch
//
//  Created by LTC-04410-0016 on 2024/2/6.
//

import SwiftUI

struct ContentView: View {
    @State private var selectedDate = Date()
    @State private var isDatePickerShown = false
    
    var body: some View {
        VStack {
            TextField("Select a date", text: .constant(""))
                .onTapGesture {
                    // 點擊 TextField 時顯示 DatePicker
                    isDatePickerShown = true
                }
            
            // 用 .sheet 顯示 DatePicker 彈出視窗
            .sheet(isPresented: $isDatePickerShown) {
                VStack {
                    DatePicker("Select a date", selection: $selectedDate, displayedComponents: .date)
                        .datePickerStyle(WheelDatePickerStyle())
                        .labelsHidden()
                    
                    Button("Done") {
                        // 在彈出視窗中點擊 Done 按鈕後關閉視窗
                        isDatePickerShown = false
                    }
                }
                .padding()
            }
            
            Text("Selected Date: \(selectedDate)")
                .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
