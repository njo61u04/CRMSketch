//
//  VerifyPage.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/9.
//

import SwiftUI

struct MailGunTemplatePage: View {
    @State private var pass = false
    @State private var loginProvider: PostObject.LoginProvider = .email
    @State private var stateToken = ""
    
    var body: some View {
        NavigationStack {
            VStack {
                Text("Email")
                    .titleStyle()
                
                HStack {
                    VStack(alignment: .leading){
                        Text("雄獅註冊驗證信")
                            .contentStyle()
                        Text("XXX你好")
                            .contentStyle()
                            .padding(.top, heightFix * 24)
                        HStack {
                            Text("驗證連結：")
                                .contentStyle()
                            Button {
                                communicator.fetchPostResponse(
                                    urlString: api.apiBase + api.verifyAccountAPI,
                                    parameters: PostObject(verification_token: ""/*待填寫*/)
                                ) { result, error in
                                    if let error = error {
                                        print("Fail to Preverify, Error: \(error)")
                                    }
                                    guard let result = result else {
                                        return
                                    }
                                    if let token = result.data.token {
                                        stateToken = token
                                    }
                                }
                                pass = true
                            } label: {
                                Text("http://....")
                                    .contentStyle()
                                    .underline()
                                    .foregroundColor(Color(red: 0.05, green: 0.6, blue: 1))
                            }
                        }
                        .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
                        Spacer()
                    }
                    Spacer()
                }
                .padding(.leading, widthFix * 38)
                .padding(.top, heightFix * 15.64)
                Spacer()
            }
            .padding(.top, heightFix * 55)
        }
        .navigationDestination(isPresented: $pass) {
            RegisterDataPage(token: $stateToken, loginProvider: $loginProvider)
        }
    }
}

#Preview {
    MailGunTemplatePage()
}
