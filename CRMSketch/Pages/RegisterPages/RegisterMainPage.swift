//
//  RegisterButton.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/8.
//

import SwiftUI



struct RegisterMainPage: View {
    var body: some View {
//        NavigationStack {
            VStack {
                Text("註冊")
                    .titleStyle()
                    .padding(.bottom, heightFix * 39.27)
                VStack(spacing: widthFix * 16) {
                    NavigationLink {
                        RegisterEmailPage(titleText: .constant("註冊"), loginProvider: .constant(.email))
                    } label: {
                        EmailButton(method: .constant("註冊"))
                    }
                    NavigationLink {
                        RegisterEmailPage(titleText: .constant("取得 email"), loginProvider: .constant(.facebook))
                    } label: {
                        FacebookButton(method: .constant("註冊"))
                    }
                    GmailButton(method: .constant("註冊"))
                    AppleButton(method: .constant("註冊"))
                    LineButton(method: .constant("註冊"))
                }
                Spacer()
            }
            .padding(.top, heightFix * 100)
//        }
    }
}

#Preview {
    RegisterMainPage()
}
