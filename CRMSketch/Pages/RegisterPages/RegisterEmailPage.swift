//
//  RegisterEmailPage.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/8.
//

import SwiftUI



struct RegisterEmailPage: View {
    @State private var email: String = ""
    @State private var pass = false
    @State private var showAlert = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    @State var token: String = ""
    @Binding var titleText: String
    @Binding var loginProvider: PostObject.LoginProvider
    
    var body: some View {
        NavigationStack{
            ScrollView {
                VStack {
                    Text(titleText)
                        .titleStyle()
                        .padding(.bottom, heightFix * 65.54)
                    
                    EmailRow(email: $email)
                        .padding(.bottom, heightFix * 81.82)
                    
                    Button {
                        communicator.fetchPostResponse(
                            urlString: api.apiBase + api.preVerifyAccountAPI,
                            parameters: PostObject(
                                login_provider: loginProvider, 
                                email: email
                            )
                        ) { result, error in
                            if let error = error {
                                print("Fail to Preverify, Error: \(error)")
                            }
                            guard let result = result else {
                                return
                            }
                            if let newMember = result.data.is_new_member,
                               let token = result.data.verification_token {
                                self.token = token
                                if newMember {
                                    communicator.fetchPostResponse(
                                        urlString: api.apiBase + api.verifyAccountAPI,
                                        parameters: PostObject(verification_token: token)
                                    ) { result, error in
                                        if let error = error {
                                            print("Fail to Preverify, Error: \(error)")
                                        }
                                        guard let result = result else {
                                            return
                                        }
                                        pass = true
                                        showAlert = false
                                        alertTitle = ""
                                        alertMessage = ""
                                    }
                                    
                                } else {
                                    pass = false
                                    showAlert = true
                                    alertTitle = "此email已註冊過"
                                    alertMessage = "前往登入頁"
                                }
                            } else {
                                let code = result.status.code
                                if code == "MB01003" {
                                    pass = false
                                    showAlert = true
                                    alertTitle = "Validation failed."
                                } else if code == "MB03002" {
                                    pass = false
                                    showAlert = true
                                    alertTitle = "Too many request to send a verification email."
                                    alertMessage = "Please wait 30 seconds."
                                } else if code == "MB01001" {
                                    pass = false
                                    showAlert = true
                                    alertTitle = "Internal server error."
                                }
                            }
                        }
                    }label: {
                        SubmitButton()
                    }
                    
                    .alert(alertTitle, isPresented: $showAlert, actions: {
                        //...
                    }, message: {
                        Text(alertMessage)
                    })
                    Spacer()
                }
                .padding(.top, heightFix * 55)
            }
        }
        .scrollDismissesKeyboard(.interactively)
        .navigationDestination(isPresented: $pass) {//這個掛哪裡差很多，如果掛在button會發生按back直接回到原本的頁面(就算是在第三或第四頁也一樣)，但如果掛在NavigationStack的話就可以一層一層返回
            RegisterJumpToEmailPage(url: .constant(api.apiBase + api.preVerifyAccountAPI), email: $email, parameters: .constant(PostObject(login_provider: .email, email: email)))
            
        }
    }
}

#Preview {
    RegisterEmailPage(titleText: .constant("註冊"), loginProvider: .constant(.email))
}
