//
//  RegisterPage.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/10.
//

import SwiftUI

struct RegisterDataPage: View {
    @StateObject var bindingObject = CreateMemberObservableObject()
    @State private var dataFormatCorrect: Bool = false
    @State private var showAlert: Bool = false
    @Binding var token: String
    @Binding var loginProvider: PostObject.LoginProvider
    
    var body: some View {
        ScrollView {
            VStack {
                Text("註冊資料")
                    .titleStyle()
                VStack(spacing: heightFix * 16.82){
                    LastNameRow(lastName: $bindingObject.lastName)
                    
                    FirstNameRow(firstName: $bindingObject.firstName)
                    
                    PhoneRow(phoneNumber: $bindingObject.phoneNumber, internationalCode: $bindingObject.countryCallCode)
                    
                    BirthDateRow(date: $bindingObject.date)
                    
                    CreatePasswordRow(password: $bindingObject.password)
                    
                    GenderRow(sexual: $bindingObject.gender)
                    
                    CountryRow(country: $bindingObject.nationalityCountryID)
                    
                    AddressRow(addressCountry: $bindingObject.countryID, addressCity: $bindingObject.city, addressDetail: $bindingObject.addressDetail)
                    
                }
                .padding(.top, 25)
                
                Button {
                    var method: String = ""
                    switch loginProvider {
                    case .email:
                        method = "email"
                    case .facebook:
                        method = "facebook"
                    case .google:
                        method = "google"
                    case .apple:
                        method = "apple"
                    case .line:
                        method = "line"
                    }
                    
                    let registerMethodAPI = api.registerAPI.replaceProvider(method: method)
                    
                    communicator.fetchPostResponse(
                        urlString: api.apiBase + registerMethodAPI,
                        parameters:
                            PostObject(
                                verification_token: token,
                                user_access_token: ""/*待填寫*/,
                                password: bindingObject.password,
                                confirm_password: bindingObject.password,
                                first_name: bindingObject.firstName,
                                last_name: bindingObject.lastName,
                                contact_mobile:
                                    PostObject.ContactMobile(
                                        country_call_code: bindingObject.countryCallCode,
                                        number: bindingObject.phoneNumber
                                    ),
                                birth_date: bindingObject.date.description.dateFormatFix,
                                gender: bindingObject.gender,
                                nationality_country_id: Int(bindingObject.nationalityCountryID) ?? 0,
                                address:
                                    PostObject.Address(
                                        country_id: Int(bindingObject.countryID) ?? 0,
                                        city_name: bindingObject.city,
                                        address: bindingObject.addressDetail
                                    ),
                                service_terms: []/*待填寫*/
                            )
                    ) { result, error in
                        if let error = error {
                            print("Fail to Post Data, Error: \(error)")
                        }
                        guard let result = result else {
                            assertionFailure("Fail to Get Response Data")
                            return
                        }
                    }
                    print(api.apiBase + registerMethodAPI)
                } label: {
                    SubmitButton()
                }
                .padding(.top, heightFix * 25.27)
                .alert("請輸入完整的資料", isPresented: $showAlert) {
                    Button("OK") {
                        //...
                    }
                }
                Spacer()
            }
            .padding(.top, heightFix * 55)
        }
    }
}

#Preview {
    RegisterDataPage(token: .constant(""), loginProvider: .constant(.email))
}
