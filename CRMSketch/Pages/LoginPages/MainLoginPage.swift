//
//  LoginMainPage.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/19.
//

import SwiftUI

struct MainLoginPage: View {
    @Binding var path: NavigationPath

    var body: some View {
//        NavigationStack {
            VStack {
                Text("登入")
                    .titleStyle()
                    .padding(.bottom, heightFix * 39.27)
                VStack(spacing: heightFix * 16) {
                    Button("使用 Email 登入") {
                        path.append(HomePage.ViewOptions.register)
                    }
                    .uniformButtonStyle()
                    Button("使用 Facebook 登入") {
                        path = .init()
                    }
                    .uniformButtonStyle()
                    Button("使用 Google 登入") {
                        path = .init()
                    }
                    .uniformButtonStyle()
                    Button("使用 Line 登入") {
                        path = .init()
                    }
                    .uniformButtonStyle()
                    Button("使用 Apple 登入") {
                        path = .init()
                    }
                    .uniformButtonStyle()
                }
                Spacer()
            }
            .padding(.top, heightFix * 100)
//        }
    }
}

#Preview {
    MainLoginPage(path: .constant(NavigationPath()))
}
