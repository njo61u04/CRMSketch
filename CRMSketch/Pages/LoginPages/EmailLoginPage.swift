//
//  EmailLoginPage.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/19.
//

import SwiftUI

struct EmailLoginPage: View {   
    @State private var account: String = ""
    @State private var password: String = ""
    @State private var showAlert = false
    @State private var pass = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    @State private var forgetPassword = false

    
    var body: some View {
        NavigationStack {
            ScrollView {
                VStack {
                    Text("Email 登入")
                        .titleStyle()
                        .padding(.bottom, heightFix * 70.54)
                    AccountLoginRow(account: $account)
                        .padding(.bottom)
                    PasswordRow(password: $password)
                        .padding(.bottom, heightFix * 81.82)
                    
                    Button {
                        communicator.fetchPostResponse(
                            urlString: api.apiBase + api.emailLoginAPI,
                            parameters: PostObject(email: account, password: password)
                        ) { result, error in
                            if let error = error {
                                print(error)
                            }
                            guard let result = result else {
                                return
                            }
                            
                            if let newMember = result.data.is_new_member,
                               let token = result.data.verification_token {
                                if newMember {
                                    pass = true
                                    showAlert = false
                                    alertTitle = ""
                                    alertMessage = ""
                                } else {
                                    pass = false
                                    showAlert = true
                                    alertTitle = "此email已註冊過"
                                    alertMessage = "前往登入頁"
                                }
                            } else {
                                if result.status.code == "MB01003" {
                                    pass = false
                                    showAlert = true
                                    alertTitle = "帳號密碼格式錯誤"
                                    alertMessage = "再輸入一次"
                                } else if result.status.code == "MB03002" {
                                    pass = false
                                    showAlert = true
                                    alertTitle = "輸入次數過多"
                                    alertMessage = "30秒後再次輸入"
                                } else if result.status.code == "MB01001" {
                                    pass = false
                                    showAlert = true
                                    alertTitle = "系統錯誤"
                                    alertMessage = ""
                                }
                            }
                        }
                    } label: {
                        SubmitButton()
                            .padding(.bottom)
                    }
                    .alert(alertTitle, isPresented: $showAlert, actions: {
                        
                    }, message: {
                        Text(alertMessage)
                    })
                    
                    HStack {
                        Button {
                            forgetPassword = true
                        } label: {
                            Text("忘記密碼？")
                                .contentStyle()
                                .foregroundStyle(Color(red: 0.05, green: 0.6, blue: 1))
                                .padding()
                        }
                        .navigationDestination(isPresented: $forgetPassword) {
                            ForgetPasswordPage()
                        }
                        Spacer()
                    }
                    Spacer()
                }
                .padding(.top, heightFix * 55)
            }
            .scrollDismissesKeyboard(.interactively)
        }
        .navigationDestination(isPresented: $pass) {
            
        }
    }
}

#Preview {
    EmailLoginPage()
}
