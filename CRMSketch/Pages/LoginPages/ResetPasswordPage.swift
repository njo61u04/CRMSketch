//
//  RestPasswordPage.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/1/25.
//

import SwiftUI

struct ResetPasswordPage: View {
    @State private var password: String = ""
    @State private var confirmPassword: String = ""
    @State private var pass = false
    @State private var showAlert = false
    @State private var alertTitle = ""
    @State private var alertMessage = ""
    @State private var token = ""
    
    var body: some View {
        NavigationStack {
            ScrollView {
                VStack {
                    Text("重設密碼")
                        .titleStyle()
                        .padding(.bottom, heightFix * 70.54)
                    NewPasswordRow(password: $password)
                        .padding(.bottom)
                    ConfirmPasswordRow(password: $password)
                        .padding(.bottom, heightFix * 81.82)
                    
                    Button {
                        communicator.fetchPutResponse(
                            urlString: api.apiBase + api.resetPasswordAPI,
                            parameters: PostObject(
                                verification_token: "123",
                                password: password,
                                confirm_password: confirmPassword
                            )
                        ) { result, error in
                            if let error = error {
                                print("Fail to Post Data, Error: \(error)")
                            }
                            guard let result = result else {
                                assertionFailure("Fail to Get Response Data!!")
                                return
                            }
                            
                            if let token = result.data.token {
                                pass = true
                                communicator.fetchPostResponse(
                                    urlString: api.apiBase + api.verifyForgetPasswordAPI,
                                    parameters: PostObject(verification_token: token)
                                ) { result, error in
                                    if let error = error {
                                        print("Fail to Post Data, Error: \(error)")
                                    }
                                    guard let result = result else {
                                        assertionFailure("Fail to Get Response Data!!")
                                        return
                                    }
                                }
                            } else {
                                let code = result.status.code
                                if code == "MB03001" {
                                    showAlert = true
                                    alertTitle = "Email address is not registered."
                                } else if code == "MB03002" {
                                    showAlert = true
                                    alertTitle = "Too many request to send a verification email."
                                    alertMessage = "Please wait 30 seconds."
                                } else if code == "MB01001" {
                                    showAlert = true
                                    alertTitle = "Internal server error."
                                } else {
                                    showAlert = true
                                    alertTitle = "Validation failed."
                                }
                            }
                        }
                        
                    } label: {
                        SubmitButton()
                            .padding(.bottom)
                    }
                    Spacer()
                }
                .padding(.top, heightFix * 55)
            }
            .scrollDismissesKeyboard(.interactively)
        }
    }
}

#Preview {
    ResetPasswordPage()
}
