//
//  ForgetPassword.swift
//  CRM
//
//  Created by LTC-04410-0016 on 2024/1/19.
//

import SwiftUI

struct ForgetPasswordPage: View {
    @State private var pass = false
    @State private var email: String = ""
    @State private var showAlert = false
    @State private var alertTitle = ""
    @State private var alertMessage = ""
    @State private var token = ""
    
    var body: some View {
        NavigationStack {
            VStack{
                Text("忘記密碼")
                    .titleStyle()
                    .padding(.bottom, heightFix * 30.27)
                
                Text("請提供您的帳號")
                    .contentStyle()
                    .padding(.bottom, heightFix * 15.75)
                
                TextField("", text: $email, axis: .vertical)
                    .frame(width: widthFix * 201, height: heightFix * 30.15)
                    .textFieldStyle(GrayBorder())
                    .keyboardType(.emailAddress)
                    .textInputAutocapitalization(.never)
                    .font(Font.custom("Inter", size: 10))
                    .foregroundColor(Color(red: 0.18, green: 0.21, blue: 0.28))
                
                
                Button(action: {
                    communicator.fetchPostResponse(
                        urlString: api.apiBase + api.forgetPasswordAPI,
                        parameters: PostObject(email: email)) { result, error in
                        if let error = error {
                            print("Fail to Post Data, Error: \(error)")
                        }
                        guard let result = result else {
                            assertionFailure("Fail to Get Response Data!!")
                            return
                        }
                        
                        if let token = result.data.token {
                            pass = true
                            communicator.fetchPostResponse(
                                urlString: api.apiBase + api.verifyForgetPasswordAPI,
                                parameters: PostObject(verification_token: token)
                            ) { result, error in
                                if let error = error {
                                    print("Fail to Post Data, Error: \(error)")
                                }
                                guard let result = result else {
                                    assertionFailure("Fail to Get Response Data!!")
                                    return
                                }
                            }
                        } else {
                            let code = result.status.code
                            if code == "MB03001" {
                                showAlert = true
                                alertTitle = "Email address is not registered."
                            } else if code == "MB03002" {
                                showAlert = true
                                alertTitle = "Too many request to send a verification email."
                                alertMessage = "Please wait 30 seconds."
                            } else if code == "MB01001" {
                                showAlert = true
                                alertTitle = "Internal server error."
                            } else {
                                showAlert = true
                                alertTitle = "Validation failed."
                            }
                        }
                    }
                }, label: {
                    SubmitButton()
                })
                .padding(.top, heightFix * 119.1)
                .alert(alertTitle, isPresented: $showAlert, actions: {
                    
                }, message: {
                    Text(alertMessage)
                })
                
                Spacer()
            }
            .padding(.top, heightFix * 55)
        }
        .navigationDestination(isPresented: $pass) {
            ForgetJumpToEmailPage(url: .constant(api.apiBase + api.forgetPasswordAPI), email: $email)
        }
    }
}

#Preview {
    ForgetPasswordPage()
}
