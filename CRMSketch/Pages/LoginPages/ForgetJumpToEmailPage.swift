//
//  ForgetJumpToEmailPage.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/1/24.
//

import SwiftUI

struct ForgetJumpToEmailPage: View {
    @State private var pass = false
    @State private var showAlert = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    @Binding var url: String
    @Binding var email: String

    var body: some View {
        NavigationStack {
            VStack {
                Text("忘記密碼")
                    .titleStyle()
                    .padding(.bottom, heightFix * 71.77)
                
                VStack(alignment: .leading) {
                    Text("驗證信已寄出，請至您的")
                        .contentStyle()
                    
                    Button(action: {
                        pass = true
                    }, label: {
                        Text(email)
                            .contentStyle()
                            .foregroundColor(.blue)
                            .underline()
                    })
                    .navigationDestination(isPresented: $pass) {
                        MailGunTemplatePage()
                    }
                    
                    Text("中點擊驗證連結")
                        .contentStyle()
                }
                .foregroundColor(Color(red: 0.12, green: 0.12, blue: 0.12))
                .frame(width: widthFix * 281, alignment: .topLeading)
                .padding(.bottom, heightFix * 71.77)
                
                Text("尚未收到驗證信請在30秒後按下下方重新寄送驗證信")
                    .font(Font.custom("Inter", size: 12))
                    .foregroundStyle(Color.gray)

                Button {

                    communicator.fetchPostResponse(
                        urlString: url,
                        parameters: PostObject(email: email)
                    ) { result, error in
                        if let error = error {
                            print("Fail to Preverify, Error: \(error)")
                        }
                        guard let result = result else {
                            return
                        }
                        
                        if let token = result.data.verification_token {
                            communicator.fetchPostResponse(urlString: api.apiBase + api.verifyForgetPasswordAPI, parameters: PostObject(verification_token: token)) { result, error in
                                if let error = error {
                                    print("Fail to Preverify, Error: \(error)")
                                }
                                guard let result = result else {
                                    return
                                }
                                showAlert = false
                                alertTitle = ""
                                alertMessage = ""
                            }
                        }else {
                            let code = result.status.code
                            if code == "MB01003" {
                                showAlert = true
                                alertTitle = "Validation failed."
                            } else if code == "MB03002" {
                                showAlert = true
                                alertTitle = "Too many request to send a verification email."
                                alertMessage = "Please wait 30 seconds."
                            } else if code == "MB01001" {
                                showAlert = true
                                alertTitle = "Internal server error."
                            }
                        }

                    }
                } label:  {
                    ResendButton()
                }
                Spacer()
            }
            .padding(.top, heightFix * 55)
        }
    }
}

#Preview {
    ForgetJumpToEmailPage(url: .constant(""), email: .constant("abc123@gmail.com"))
}
