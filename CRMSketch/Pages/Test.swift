//
//  Test.swift
//  CRMSketch
//
//  Created by Lionuser on 2024/4/9.
//

import SwiftUI

@available(iOS 16.0, *)
struct HomePage: View {
    @State private var path: NavigationPath = .init()
    var body: some View {
        NavigationStack(path: $path){
            Button("登入") {
                
            }
            .uniformButtonStyle()
            .navigationDestination(for: ViewOptions.self) { option in
                option.view($path)
            }
        }
    }
    //Create an `enum` so you can define your options
    enum ViewOptions{
        case userTypeView
        case register
        //Assign each case with a `View`
        @ViewBuilder func view(_ path: Binding<NavigationPath>) -> some View{
            switch self{
            case .userTypeView:
                MainLoginPage(path: path)
            case .register:
                RegisterMainPage()
            }
        }
    }
}

#Preview {
    HomePage()
}
