//
//  CheckOutPage.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/1/31.
//

import SwiftUI

struct CheckOutPage: View {
    @State private var pass = false
    
    
    var body: some View {
        NavigationStack {
            VStack {
                Button {
                    communicator.fetchGetResponse(
                        urlString: api.apiBase + api.getMemberData,
                        bearerToken: ""/*待填寫*/
                    ) { result, error in
                        if let error = error {
                            print("Fail to Get Member Data, Error: \(error)")
                        }
                        guard let result = result else {
                            return
                        }
                    }
                } label: {
                    CheckMemberDataButton()
                        .padding(.top, heightFix * 239)
                }
                Spacer()
            }
        }
        .navigationDestination(isPresented: $pass) {
            MemberDataPage()
        }
    }
}

#Preview {
    CheckOutPage()
}
