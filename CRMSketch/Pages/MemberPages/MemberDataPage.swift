//
//  MemberDataPage.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/1/31.
//

import SwiftUI

struct MemberDataPage: View {
    var body: some View {
        VStack {
            Text("會員中心")
                .titleStyle()
        }
    }
}

#Preview {
    MemberDataPage()
}
