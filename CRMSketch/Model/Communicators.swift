//
//  Communicators.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/1/23.
//

import Foundation
import Alamofire
import UIKit

let communicator = Communicator.shared

class Communicator {
    static let shared = Communicator()
    private init(){}

    func fetchPostResponse(urlString: String, parameters: PostObject = PostObject(), bearerToken: String = "", completion: @escaping(_ result: ResponseObject?, _ error: Error?) -> Void) {
        
        if let postData = try? JSONEncoder().encode(parameters) {
            self.prettyPrintedPost(data: postData)
        }//增加post data的可讀性
        
        let headers: HTTPHeaders = [
            .authorization(bearerToken: bearerToken),
            .contentType("application/json"),
//            .accept("application/json"),
        ]
        AF.request(urlString, method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default,
                   headers: headers
        ).responseDecodable(of: ResponseObject.self) { response in
            if let error = response.error {
                print("Fail to Post Data, Error: \(error)")
                completion(nil, error)
            } else {
                print("Post Success")
            }
            guard let data = response.data,
                  let value = response.value else {
                assertionFailure("Fail to Get Response Data")
                return
            }
            self.prettyPrintedResponse(data: data)
            completion(value, nil)
        }
    }

    func fetchPutResponse(urlString: String, parameters: PostObject = PostObject(), bearerToken: String = "", completion: @escaping(_ result: ResponseObject?, _ error: Error?) -> Void) {
        
        if let putData = try? JSONEncoder().encode(parameters) {
            self.prettyPrintedPost(data: putData)
        }//增加post data的可讀性
        
        let headers: HTTPHeaders = [
            .authorization(bearerToken: bearerToken),
            .contentType("application/json"),
            .accept("application/json")
        ]
        AF.request(urlString,
                   method: .put,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default,
                   headers: headers
        ).responseDecodable(of: ResponseObject.self) { response in
            if let error = response.error {
                print("Fail to Put Data, Error: \(error)")
                completion(nil, error)
            }
            guard let data = response.data,
                let value = response.value else {
                assertionFailure("Fail to Get Response Data")
                return
            }
            self.prettyPrintedResponse(data: data)
            completion(value, nil)
        }
    }
    
    func fetchGetResponse(urlString: String, parameters: PostObject = PostObject(), bearerToken: String = "", completion: @escaping(_ result: ResponseObject?, _ error: Error?) -> Void) {
        
        if let getData = try? JSONEncoder().encode(parameters.self) {
            prettyPrintedPost(data: getData)
        }//增加post data的可讀性
        
        let headers: HTTPHeaders = [
            .authorization(bearerToken: bearerToken),
            .contentType("application/json"),
            .accept("application/json")
        ]
        AF.request(urlString,
                   method: .get,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default,
                   headers: headers
        ).responseDecodable(of: ResponseObject.self) { response in
            if let error = response.error {
                completion(nil, error)
                print("Fail to Get Response Data, Error: \(error)")
            }
            guard let data = response.data,
                let value = response.value else {
                assertionFailure("Fail to Get Response Data!!!!")
                return
            }
            self.prettyPrintedResponse(data: data)
            completion(value, nil)
        }
    }
    
    func fetchDeleteResponse(urlString: String, parameters: PostObject = PostObject(), bearerToken: String = "", completion: @escaping(_ result: ResponseObject?, _ error: Error?) ->Void) {

        if let deleteData = try? JSONEncoder().encode(parameters) {
            self.prettyPrintedPost(data: deleteData)
        }//增加post data的可讀性
        
        let headers: HTTPHeaders = [
            .authorization(bearerToken: bearerToken),
            .contentType("application/json"),
            .accept("application/json")
        ]
        AF.request(urlString,
                   method: .delete,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default,
                   headers: headers
        ).responseDecodable(of: ResponseObject.self) { response in
            if let error = response.error {
                print("Fail to Get Response Data, Error: \(error)")
                completion(nil, error)
            }
            guard let value = response.value,
                  let data = response.data else {
                assertionFailure("Fail to Get Response Data!!!!")
                return
            }
            self.prettyPrintedResponse(data: data)
            completion(value, nil)
        }
    }
    
    func prettyPrintedPost(data: Data) {
        if let responseAny = try? JSONSerialization.jsonObject(with: data) as? [String: Any] {//先將data轉換成需要的type
            if let responseData = try? JSONSerialization.data(withJSONObject: responseAny, options: .prettyPrinted) {//再轉回成data
                /* .prettyPrinted: JSONSerialization.WritingOptions { get },
                 Sorts dictionary keys for output using [NSLocale systemLocale].
                 Keys are compared using NSNumericSearch. The specific sorting method used is subject to change.*/
                if let responseJSON = String(data: responseData, encoding: .utf8) {//再轉換成string
                    print("Post Data:\n \(responseJSON)")
                }else {
                    print("Fail to Turn Data Into String")
                }
            }else {
                print("Fail to Turn [String: Any] Into Data")
            }
        }else {
            print("Fail to Turn Data Into [String: Any]")
        }
    }
    
    func prettyPrintedResponse(data: Data) {
        if let responseAny = try? JSONSerialization.jsonObject(with: data) as? [String: Any] {
            if let responseData = try? JSONSerialization.data(withJSONObject: responseAny, options: .prettyPrinted) {
                if let responseJSON = String(data: responseData, encoding: .utf8) {
                    print("Response Data:\n \(responseJSON)")
                }else {
                    print("Fail to Turn Data Into String")
                }
            }else {
                print("Fail to Turn [String: Any] Into Data")
            }
        }else {
            print("Fail to Turn Data Into [String: Any]")
        }
    }
}
