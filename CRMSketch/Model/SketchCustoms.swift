//
//  UICustomer.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/1/29.
//

import Foundation
import SwiftUI

let heightFix = UIScreen.main.bounds.height / 812
let widthFix = UIScreen.main.bounds.width / 375

//Cusom TextFieldStyle
struct GrayBorder: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding(7.5)
            .overlay(
                RoundedRectangle(cornerRadius: 3.76785)
                    .stroke(Color(red: 0.8, green: 0.82, blue: 0.88), lineWidth: 1.25)
            )
    }
}

//View+Extension
extension View {
    func titleStyle() -> some View {
        self
            .font(
                .custom("Inter", size: 64)
                .weight(.medium)
            )
            .frame(height: 96)
            .foregroundStyle(Color(red: 0.12, green: 0.12, blue: 0.12))
    }
    
    func contentStyle() -> some View {
        self
            .font(
                .custom("Inter", size: 24)
                .weight(.medium)
            )
    }
    
    func uniformButtonStyle() -> some View {
        self
            .font(
                Font.custom("Inter", size: 16)
                    .weight(.medium)
            )
            .foregroundColor(.white)
            .frame(width: 186, height: 48)
            .background(Color(red: 0.18, green: 0.21, blue: 0.28))
            .clipShape(RoundedRectangle(cornerRadius: 6))
    }
    
    func otherButtonStyle() -> some View {
        self
            .font(
                Font.custom("Inter", size: 16)
                    .weight(.medium)
            )
            .foregroundColor(.white)
            .padding(.vertical, 12)
            .padding(.horizontal, 20)
            .background(Color(red: 0.18, green: 0.21, blue: 0.28))
            .clipShape(RoundedRectangle(cornerRadius: 6))
    }
}

//String+Extension
extension String {
    var yearFix: String {
        String(self
            .prefix(4))
    }
    
    var monthFix: String {
        String(self
            .prefix(7)
            .suffix(2)
        )
    }
    
    var dayFix: String {
        String(self
            .prefix(10)
            .suffix(2)
        )
    }
    
    var dateFormatFix: String {
        String(self
            .prefix(10)
            .replacingOccurrences(of: "-", with: "/")
        )
    }

    func replaceProvider(method: String) -> String {
        self.replacingOccurrences(of: "{provider}", with: method)
    }
}
