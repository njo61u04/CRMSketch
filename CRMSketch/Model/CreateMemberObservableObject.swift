//
//  CreateMemberDataObserveObject.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/2/2.
//

import Foundation

class CreateMemberObservableObject: ObservableObject {
    @Published var lastName: String = ""
    @Published var firstName: String = ""
    @Published var phoneNumber: String = ""
    @Published var countryCallCode: String = ""
    @Published var date = Date()
    @Published var password: String = ""
    @Published var gender: String = ""
    @Published var nationalityCountryID: String = ""
    @Published var countryID: String = ""
    @Published var city: String = ""
    @Published var addressDetail: String = ""
}
