//
//  APIs.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/1/23.
//

import Foundation

let api = APIs()

struct APIs {
    
    let apiBase = "https://member-service.sandbox.asiayo.com"
    
    //Authenticating
    let emailLoginAPI = "/api/lion/v1/login/email"
    let facebookLoginAPI = "/api/lion/v1/login/facebook"
    let googleLoginAPI = "/api/lion/v1/login/google"
    let lineLoginAPI = "/api/lion/v1/login/line"
    let appleLoginAPI = "/api/lion/v1/login/apple"
    let emailBindingAPI = "/api/lion/v1/login/{provider}/email-binding"
    let logoutAPI = "/api/lion/v1/logout"
    
    //Campaign
    let searchCampaigns = "/api/lion/v1/campaigns"
    
    //Country
    let getCountries = "/api/lion/v1/countries"
    let getCurrencies = "/api/lion/v1/currencies"
    
    //Coupon
    let createCouponConsolidate = "/api/lion/v1/coupon/consolidate"
    let applyCoupon = "/api/lion/v1/coupon/apply"
    let checkaCouponAvailability = "/api/lion/v1/coupon/availability"
    let lockCoupon = "/api/lion/v1/coupon/lock"
    let releaseCoupon = "/api/lion/v1/coupon/release"
    let revokeUsedCoupon = "/api/lion/v1/coupon/revoke"
    
    //Forget Password
    let forgetPasswordAPI = "/api/lion/v1/forget-password"
    let resetPasswordAPI = "/api/lion/v1/reset-password"
    let verifyForgetPasswordAPI = "/api/lion/v1/forget-password/verify"
    let forgetPasswordVerifyStatusAPI = "/api/lion/v1/forget-password/verify-status"
    
    //Member
    let getMemberData = "/api/lion/v1/member"
    let updateMemberData = "/api/lion/v1/member"
    let deleteMemberData = "/api/lion/v1/member"
    let getMemberCoupons = "/api/lion/v1/member/coupons"
    
    //Register
    let registerAPI = "/api/lion/v1/member/{provider}/register"
    
    //Verification Account
    let preVerifyAccountAPI = "/api/lion/v1/member/account/email"
    let verifyAccountAPI = "/api/lion/v1/member/account/email/verify"
    let getVerifyAccountStatusAPI = "/api/lion/v1/member/account/email/verify-status"
    let sendMobileCodeAPI = "/api/lion/v1/member/account/mobile"
    let verifyMobileAPI = "/api/lion/v1/member/account/mobile/verify"
}
