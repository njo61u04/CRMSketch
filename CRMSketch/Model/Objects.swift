//
//  Objects.swift
//  MemberServiceWithAsiaYo
//
//  Created by LTC-04410-0016 on 2024/1/23.
//

import Foundation

import Foundation

struct PostObject: Codable {
    
    var login_provider: LoginProvider?
    enum LoginProvider: String, Codable {
        case email = "email"
        case facebook = "facebook"
        case google = "google"
        case apple = "apple"
        case line = "line"
    }
    
    var email: String?
    var verification_token: String?
    var user_access_token: String?
    var password: String?
    var confirm_password: String?
    var first_name: String?
    var last_name: String?
    
    var contact_mobile: ContactMobile?
    struct ContactMobile: Codable {
        var country_call_code: String
        var number: String
    }
    
    var birth_date: String?
    var gender: String?
    var nationality_country_id: Int?
    
    var address: Address?
    struct Address: Codable {
        var country_id: Int
        var city_name: String
        var address: String
    }
    
    var service_terms: [ServiceTerms]?
    struct ServiceTerms: Codable {
        var name: String
        var is_accepted: Bool
    }

    var verification_code: String?
    var nickname: String?
    var country_call_code: String?
    var number: String?
    var locale: String?
    var backup_email: String?
    
    var passport: Passport?
    struct Passport: Codable {
        var surname: String
        var given_name: String
        var number: String
    }

    var pid_is_foreign: Bool?
    var pid_number: String?
    var mtp_id_number: String?
}

struct ResponseObject: Codable {
    var status: Status
    var data: AccountObject

    struct Status: Codable {
        var code: String
        var msg: String
    }
    
    struct AccountObject: Codable {
        var token: String?
        var is_new_member: Bool?
        var verification_token: String?
        var expires_at: Int?
        
        var errors: Errors?
        struct Errors: Codable {
            var field1: [String]?
            var field2: [String]?
            
            enum error: String, CodingKey {
                case field1 = "field-1"
                case field2 = "field-2"
            }
        }
        
        var email: String?
        var verify_status: Bool?
        var member: String?
        var id: Int?
        var first_name: String?
        var last_name: String?
        var nickname: String?
        var backup_email: String?
        
        var contact_mobile: ContactMobile?
        struct ContactMobile: Codable {
            var country_call_code: Int
            var number: Int
        }
        
        var birth_date: String?
        var register_date: String?
        var status: Int?
        var is_verify: Bool?
        var gender: String?
        var nationality_id: Int?
        
        var address: Address?
        struct Address: Codable {
            var country_id: Int
            var city_name: String
            var address: String
        }
        
        var locale: String?
        
        var passport: Passport?
        struct Passport: Codable {
            var surname: String
            var given_name: String
            var number: Int
        }
        
        var pid_number: String?
        var pid_is_foreign: Bool?
        var mtp_id_number: String?
        
        var service_terms: [ServiceTerms]?
        struct ServiceTerms: Codable {
            var name: String
            var is_accepted: Bool
        }
    }
}
